<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

function getRoute()
{

    $id = filter_input(INPUT_POST, "id");

    // On vérifie que le login existe bien
    $pdo = new PDO("mysql:host=localhost:3306;dbname=points", "root", "root");
    $stmt = $pdo->prepare("SELECT * FROM route WHERE idroute = :id");
    $stmt->execute([
        ":id" => $id
    ]);
    $route = $stmt->fetch(PDO::FETCH_ASSOC);

    // Si l'on n'a trouvé personne dans la BDD
    if(!$route)
    {
        return [
            "status" => "error",
            "message" => "Aucune route trouvée"
        ];
    }

    //Transform string into array of coord
    $points = array();
    $array = explode(",LatLng", $route["points"]);
    foreach ($array as $point){
        preg_match_all('/-?\d+\.\d+/', $point, $matches);
        array_push($points, $matches[0]);
    }

    //Retourne les infos de connexion si tout est bon
    return [
        "status" => "success",
        "route" => $route,
        "points" => $points
    ];
}

$data = getRoute();
header('Content-Type: application/json');
echo json_encode($data);