-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 17 juil. 2023 à 02:32
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `points`
--
CREATE DATABASE IF NOT EXISTS `points` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `points`;

-- --------------------------------------------------------

--
-- Structure de la table `route`
--

DROP TABLE IF EXISTS `route`;
CREATE TABLE IF NOT EXISTS `route` (
  `idroute` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `points` longtext NOT NULL,
  PRIMARY KEY (`idroute`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `route`
--

INSERT INTO `route` (`idroute`, `name`, `points`) VALUES
(1, 'TEST', 'LatLng(48.885037, 2.031784),LatLng(48.808672, 2.201385),LatLng(48.808672, 2.201385),LatLng(48.798723, 2.618866),LatLng(48.798723, 2.618866),LatLng(48.894518, 2.721176)'),
(2, 'TEST 2', 'LatLng(48.799175, 2.227135),LatLng(48.936033, 2.333565),LatLng(48.936033, 2.333565),LatLng(48.799627, 2.445488),LatLng(48.799627, 2.445488),LatLng(48.9049, 2.196922),LatLng(48.9049, 2.196922),LatLng(48.895873, 2.465401),LatLng(48.895873, 2.465401),LatLng(48.799627, 2.228508)');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
