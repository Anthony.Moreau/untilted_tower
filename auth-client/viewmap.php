<?php
require_once('./header.php');
require 'vendor/autoload.php';
header("Access-Control-Allow-Origin: *");
session_start();

$points = [];
function getRoute()
{

    $method = $_SERVER["REQUEST_METHOD"];
    if ($method == "GET") {
        $mapId = filter_input(INPUT_GET, "id");

        // S'il manque des champs, on arrête
        if (!$mapId) {
            return [
                "status" => "error",
                "message" => "No ID provided"
            ];
        }

        // Là, il faudra appeler le serveur d'authentification
        $connexion = appelServeurMap($mapId);


        // Le résultat est soit vrai, soit faux.
        if ($connexion["status"] == "error") {
            return $connexion;
        }
        return $connexion;
    }
}

function appelServeurMap($mapId)
{
    $client = new \GuzzleHttp\Client();
    $response = $client->request('POST', 'http://localhost:4002/get_route.php', [
        'form_params' => [
            'id' => $mapId
        ]
    ]);
    $body = $response->getBody();
    return json_decode($body, true);
}

$data = getRoute();

?>
<div style="display: flex; flex-direction: column; align-items: center; justify-content: space-between;">
    <h1 class="title is-1"><?= $data["route"]["name"] ?></h1>
    <div id="carte" style="margin-top: 10px;">
        <div id="map" class="box"></div>
    </div>
    <button onclick="generatePDF()">DL</button>
    <span class="tag is-info is-light" id="message"></span>
</div>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
<script>
    var array_points;

    let formData = new FormData();

    formData.append("id", <?php echo filter_input(INPUT_GET, "id") ?>);

    // console.log(formData);
    // exit();
    fetch('http://localhost:4002/get_route.php', {
            method: 'POST',
            mode: 'cors',
            body: formData
        })
        .then(response => response.json())
        .then(
            data => {
                console.log("data fetched", data);
                array_points = data["points"];
                initMap(array_points);
            }
        )

    function generatePDF() {
        var doc = new jsPDF(); //create jsPDF object
        doc.fromHTML(document.getElementById("carte"), // page element which you want to print as PDF
            15,
            15, {
                'width': map.width //set width            
            },
            function(a) {
                doc.save("HTML2PDF.pdf"); // save file name as HTML2PDF.pdf
            });
    }

    function initMap(array_points) {
        console.log("array_points", array_points);
        var center = [48.866667, 2.333333];
        var map = L.map('map').setView(center, 11);

        // Récupérer les données des stations Vélib
        fetch('https://opendata.paris.fr/api/records/1.0/search/?dataset=velib-disponibilite-en-temps-reel&q=&facet=name&facet=is_installed&facet=is_renting&facet=is_returning&facet=nom_arrondissement_communes&rows=100')
            .then(response => response.json())
            .then(data => {
                if (data && data.records) {
                    stations = data.records;
                    console.log(stations)

                    // Parcourir les stations et ajouter des marqueurs sur la carte
                    stations.forEach(station => {

                        var velibIcon = L.icon({ //add this new icon
                            iconUrl: 'https://th.bing.com/th/id/R.0218fd05bca7179c4c72bfe6e033ece4?rik=T4Vl6abrMTJjCw&pid=ImgRaw&r=0',

                            iconSize: [30, 30], // size of the icon
                        });

                        var stationName = station.fields.name + "<br><strong>capacity :</strong>" + station.fields.capacity;
                        var stationLat = station.fields.coordonnees_geo[0];
                        var stationLng = station.fields.coordonnees_geo[1];

                        var marker = L.marker([stationLat, stationLng], {}).setIcon(velibIcon).addTo(map);
                        marker.bindPopup(stationName);
                    });
                }
            })
            .catch(error => {
                console.log('Erreur lors de la récupération des données :', error);
            });


        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '© OpenStreetMap contributors'
        }).addTo(map);

        var pointList = [];
        for (let index = 0; index < array_points.length; index++) {
            console.log(new L.LatLng(array_points[index][0], array_points[index][1]));
            pointList.push(new L.LatLng(array_points[index][0], array_points[index][1]));
        }

        console.log("pointList", pointList);

        var firstpolyline = new L.Polyline(pointList, {
            color: 'red',
            weight: 3,
            opacity: 0.5,
            smoothFactor: 1
        });
        firstpolyline.addTo(map);
    }
</script>
<?php
require_once('./footer.php');
?>